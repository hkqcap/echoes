#!/usr/bin/env python2.7
"""
phenom_echoes_waveform_nakano.py: Generating phenomenological waveforms of echoes of gravitational wave as described in arXiv:1704.07175 by Nakano et al 
Copyright (C) 2017  Rico Ka Lok Lo

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

"""
Reference:
arXiv:1704.07175
"""
import numpy as np
from convert_SI_geometrized_unit import convert_mass_SI_to_geometrized

def props(template, required_args, **kwargs):
    """
    Returning a dictionary built from the combination of kwargs and the attributes of the given objects
    """
    parameters = {}

    # Updating the parameters dictionary with template and kwargs
    parameters.update(template)
    parameters.update(kwargs)

    # Checking required arguments
    missing_arguments = [required for required in required_args if required not in parameters.keys()]
    if len(missing_arguments) >= 1:
        print("Missing argument(s): " + ",".join(missing_arguments))
    assert missing_arguments == []

    return parameters

def is_float(var):
    if type(var) is float or isinstance(var, np.float64):
        return True
    else:
        return False

def get_fd_reflection_rate(M, f, a):
    """
    Calculating the reflection rate \sqrt(R) from the fitting formulas in arXiv:1704.07175
 
    Parameters
    ----------
    M: float
        The mass of the Kerr black hole (in solar mass)
    f: float
        frequency (in Hz)
    a: float
        The spin parameter (dimensionless)
    """
    M = convert_mass_SI_to_geometrized(M, "time")
    q = a # q should be dimensionless but Nakano et al. defined q = a/M, which has a dimension of per second
    x = 2*np.pi*M*f # Since x should be dimensionless, we can thus infer that M should be in the unit of time
    
    if f > 0:
        numerator = 1.0 + np.exp(-300*(x + 0.27 - q)) + np.exp(-28*(x - 0.125 - 0.6*q))
        denominator = numerator + np.exp(19*(x - 0.3 - 0.35*q))
    elif f < 0:
        x = np.abs(x)
        numerator = 1.0 + np.exp(-200*(x - 0.22 + 0.1*q)) + np.exp(-28*(x - 0.39 + 0.1*q))
        denominator = numerator + np.exp(16*(x - 0.383 + 0.09*q))
    else:
        raise ValueError("f cannot be zero.")

    return numerator/denominator

def get_fd_transmission_rate(M, f, a):
    """
    Calculating the transmission rate by normalization condition T^2 + R^2 = 1

    Parameters
    ----------
    See get_fd_reflection_rate()
    """
    return np.sqrt(1 - get_fd_reflection_rate(M, f, a)**2)

def get_td_horizon_waveform_model2(template = None, **kwargs):
    """
    Generating the 2nd model of horizon waveform/waveform that reflects off the boundary in time domain as described arXiv:1704.07175

    Parameters
    ----------
    template: object
        An object that has attached properties. This can be used to substitute for keyword arguments. A common example would be a row in an xml table.

    amplitude: float
        The amplitude of the horizon waveform
    alpha_bar: float
        The model parameter to control the smoothness of the onset of the QNM excitation
    omega_r: float
        The real part of the QNM frequency, corresponding to the frequency of the sinusoid
    omega_i: float
        The imaginary part of the QNM frequency, corresponding to the damping frequency (1/tau)

    f_lower: float
        The starting frequency (Hz) of the output frequency series
    delta_t: float
        The time interval between each sample
    """
    input_parameters = props(template, ["amplitude", "alpha_bar", "omega_r", "omega_i", "f_lower", "delta_t"], **kwargs)
    amplitude = input_parameters.pop("amplitude")
    alpha_bar = input_parameters.pop("alpha_bar")
    omega_r = input_parameters.pop("omega_r")
    omega_i = input_parameters.pop("omega_i")
    f_lower = input_parameters.pop("f_lower")
    delta_t = input_parameters.pop("delta_t")

    # Sanity check of parameters
    assert omega_i < 0, "Unphysical parameter: omega_i is not negative!"
    
    nyquist_frequency = (1.0/delta_t)/2.0
    time_array = np.arange(0, 1.0/nyquist_frequency, delta_t)
    
    omega = omega_r + 1.0j*omega_i # omega_{QNM}

    numerator = np.exp(-1.0j*omega*time_array)
    denominator = 1 + np.exp(-2.0*alpha_bar*np.abs(omega_i)*time_array)

    return time_array, numerator/denominator


