#!/usr/bin/env python2.7
"""
phenom_echoes_waveform_tapir.py: Generating phenomenological waveforms of echoes of gravitational wave as described in arXiv:1706.06155 by TAPIR, Caltech
Copyright (C) 2017  Rico Ka Lok Lo

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

"""
Reference:
arXiv:1706.06155
"""

import numpy as np
import copy

def props(template, required_args, **kwargs):
    """
    Returning a dictionary built from the combination of kwargs and the attributes of the given objects
    """
    parameters = {}

    # Updating the parameters dictionary with template and kwargs
    parameters.update(template)
    parameters.update(kwargs)

    # Checking required arguments
    missing_arguments = [required for required in required_args if required not in parameters.keys()]
    if len(missing_arguments) >= 1:
        print("Missing argument(s): " + ",".join(missing_arguments))
    assert missing_arguments == []

    return parameters

def is_float(var):
    if type(var) is float or isinstance(var, np.float64):
        return True
    else:
        return False

def get_fd_horizon_waveform(template = None, **kwargs):
    """
    Generating the horizon waveform in frequency domain as described in arXiv:1706.06155

    Parameters
    ----------
    template: object
        An object that has attached properties. This can be used to substitute for keyword arguments. A common example would be a row in an xml table.
    
    alpha_plus: complex
        The complex amplitude for the sinusoids at the positive QNM (Quasi-Normal Mode) frequency (i.e. \Omega_{+})
    alpha_minus: complex
        The complex amplitude for the sinusoids at the negative QNM frequency (i.e \Omega_{-})
    t_s: float
        The central start time of the quasi-normal mode
    beta: float
        The frequency width of the gaussian
    omega_r: float
        The real part of the QNM frequency, corresponding to the frequency of the sinusoid
    omega_i: float
        The imaginary part of the QNM frequency, corresponding to the damping frequency (1/tau)

    f_lower: float
        The starting frequency (Hz) of the output frequency series
    f_final: float
        The final frequency (Hz) of the output frequency series
    delta_f: float
        The frequency interval between each sample
    """
    input_parameters = props(template, ["alpha_plus", "alpha_minus", "t_s", "beta", "omega_r", "omega_i", "f_lower", "f_final", "delta_f"], **kwargs)
    
    # Extracting the input parameters
    alpha_plus = input_parameters.pop("alpha_plus")
    alpha_minus = input_parameters.pop("alpha_minus")
    t_s = input_parameters.pop("t_s")
    beta = input_parameters.pop("beta")
    omega_r = input_parameters.pop("omega_r")
    omega_i = input_parameters.pop("omega_i")
    f_lower = input_parameters.pop("f_lower")
    f_final = input_parameters.pop("f_final")
    delta_f = input_parameters.pop("delta_f")    

    # Sanity check of parameters
    assert omega_i < 0,"Unphysical parameter: omega_i is not negative!"

    frequency_array = np.arange(2*np.pi*f_lower, 2*np.pi*f_final, 2*np.pi*delta_f) # Angular frequency omega
    
    omega_plus = omega_r + 1.0j*omega_i
    omega_minus = -1.0*omega_r + 1.0j*omega_i

    term_1 = np.exp(1.0j*t_s*frequency_array)
    term_2 = np.exp((-1.0*frequency_array*frequency_array)/(2.0*beta*beta))
    term_3 = (alpha_plus)/(frequency_array - omega_plus)
    term_4 = (alpha_minus)/(frequency_array - omega_minus)
   
    frequency_array = (1.0/(2.0*np.pi)) * frequency_array # Convert back to frequency in Hz 
    return frequency_array, term_1*term_2*(term_3 + term_4)

def get_fd_total_transfer(template = None, **kwargs):
    """
    Generating the total transfer function to transform horizon waveform to echoes

    Parameters
    ----------
    r_bh: function pointer, should accept frequency as the only argument
        Reflection coefficient of the (final) black hole. In general this should be a function of frequency!
    t_bh: function pointer
        Transmission coefficient of the black hole. In general this should be a function of frequency!
    r: function pointer
        Reflection coefficient of the ECO (exotic compact objects). In general this should be a function of frequency!
    x0: float
        Surface of the ECO in tortoise coordinate x = r + 2M ln((r - 2M)/M)

    f_lower: float
        The starting frequency (Hz) of the output frequency series
    f_final: float
        The final frequency (Hz) of the output frequency series
    delta_f: float
        The frequency interval between each sample
    """
    input_parameters = props(template, ["r_bh", "t_bh", "r", "x0", "f_lower", "f_final", "delta_f"], **kwargs)
    
    # Extracting the input parameters
    r_bh = input_parameters.pop("r_bh")
    t_bh = input_parameters.pop("t_bh")
    r = input_parameters.pop("r")
    x0 = input_parameters.pop("x0")
    f_lower = input_parameters.pop("f_lower")
    f_final = input_parameters.pop("f_final")
    delta_f = input_parameters.pop("delta_f")

    # If r_bh, t_bh, r are given as a number, cast it to a lambda expression
    if is_float(r_bh):
        r_bh_vec = np.vectorize(lambda x: r_bh)
    else:
        r_bh_vec = np.vectorize(r_bh)
    
    if is_float(t_bh):
        t_bh_vec = np.vectorize(lambda x: t_bh)
    else:
        t_bh_vec = np.vectorize(t_bh)
    
    if is_float(r):
        r_vec = np.vectorize(lambda x: r)
    else:
        r_vec = np.vectorize(r)
 
    frequency_array = np.arange(2*np.pi*f_lower, 2*np.pi*f_final, 2*np.pi*delta_f)

    r_array = r_vec((1.0/(2.0*np.pi)) * frequency_array) # Getting the reflection coefficient
    term = r_array * np.exp(-2.0j*x0*frequency_array)

    numerator = t_bh_vec((1.0/(2.0*np.pi)) * frequency_array)*term
    denominator = 1.0 - r_bh_vec((1.0/(2.0*np.pi)) * frequency_array)*term

    frequency_array = (1.0/(2.0*np.pi)) * frequency_array # Convert back to frequency in Hz
    return frequency_array, numerator/denominator

def get_fd_nth_transfer(template = None, **kwargs):
    """
    Generating the nth transfer function to transform horizon waveform to echoes

    Parameters
    ----------
    r_bh: function pointer, should accept frequency as the only argument
        Reflection coefficient of the (final) black hole. In general this should be a function of frequency!
    t_bh: function pointer
        Transmission coefficient of the black hole. In general this should be a function of frequency!
    r: function pointer
        Reflection coefficient of the ECO (exotic compact objects). In general this should be a function of frequency!
    x0: float
        Surface of the ECO in tortoise coordinate x = r + 2M ln((r - 2M)/M)
    n: integer
        The number of echoes

    f_lower: float
        The starting frequency (Hz) of the output frequency series
    f_final: float
        The final frequency (Hz) of the output frequency series
    delta_f: float
        The frequency interval between each sample
    """
    input_parameters = props(template, ["r_bh", "t_bh", "r", "x0", "n", "f_lower", "f_final", "delta_f"], **kwargs)
    
    # Extracting the input parameters
    r_bh = input_parameters.pop("r_bh")
    t_bh = input_parameters.pop("t_bh")
    r = input_parameters.pop("r")
    x0 = input_parameters.pop("x0")
    n = input_parameters.pop("n")
    f_lower = input_parameters.pop("f_lower")
    f_final = input_parameters.pop("f_final")
    delta_f = input_parameters.pop("delta_f")

    # If r_bh, t_bh, r are given as a number, cast it to a lambda expression
    if is_float(r_bh):
        r_bh_vec = np.vectorize(lambda x: r_bh)
    else:
        r_bh_vec = np.vectorize(r_bh)
    
    if is_float(t_bh):
        t_bh_vec = np.vectorize(lambda x: t_bh)
    else:
        t_bh_vec = np.vectorize(t_bh)
    
    if is_float(r):
        r_vec = np.vectorize(lambda x: r)
    else:
        r_vec = np.vectorize(r)
    
    # Sanity check of parametrs
    assert type(n) is int, "Unphysical parameter: number of echoes n is not an integer!"
    assert n >= 0, "Unphysical parameter: number of echoes n is not non-negative!"

    frequency_array = np.arange(2*np.pi*f_lower, 2*np.pi*f_final, 2*np.pi*delta_f)

    r_array = r_vec((1.0/(2.0*np.pi)) * frequency_array) # Getting the reflection coefficient
    term = np.exp(-2.0j*x0*n*frequency_array)
    
    prefactor_1 = t_bh_vec((1.0/(2.0*np.pi)) * frequency_array)*r_array
    prefactor_2 = r_bh_vec((1.0/(2.0*np.pi)) * frequency_array)*r_array

    term *= (prefactor_1*(prefactor_2**(n-1)))

    frequency_array = (1.0/(2.0*np.pi)) * frequency_array # Convert back to frequency in Hz
    return frequency_array, term

def get_fd_total_echoes(template = None, **kwargs):
    """
    Generating the total echoes waveform in frequency domain as described in arXiv:1706.06155

    Parameters
    ----------
    template: object
        An object that has attached properties. This can be used to substitute for keyword arguments. A common example would be a row in an xml table.
    
    alpha_plus: complex
        The complex amplitude for the sinusoids at the positive QNM (Quasi-Normal Mode) frequency (i.e. \Omega_{+})
    alpha_minus: complex
        The complex amplitude for the sinusoids at the negative QNM frequency (i.e \Omega_{-})
    t_s: float
        The central start time of the quasi-normal mode
    beta: float
        The frequency width of the gaussian
    omega_r: float
        The real part of the QNM frequency, corresponding to the frequency of the sinusoid
    omega_i: float
        The imaginary part of the QNM frequency, corresponding to the damping frequency (1/tau)

    r_bh: function pointer, should accept frequency as the only argument
        Reflection coefficient of the (final) black hole. In general this should be a function of frequency!
    t_bh: function pointer
        Transmission coefficient of the black hole. In general this should be a function of frequency!
    r: function pointer
        Reflection coefficient of the ECO (exotic compact objects). In general this should be a function of frequency!
    x0: float
        Surface of the ECO in tortoise coordinate x = r + 2M ln((r - 2M)/M)

    f_lower: float
        The starting frequency (Hz) of the output frequency series
    f_final: float
        The final frequency (Hz) of the output frequency series
    delta_f: float
        The frequency interval between each sample
    """
    input_parameters = props(template, ["alpha_plus", "alpha_minus", "t_s", "beta", "omega_r", "omega_i", "r_bh", "t_bh", "r", "x0", "f_lower", "f_final", "delta_f"], **kwargs)
    
    horizon_params = copy.deepcopy(input_parameters) # Make a deep copy of input parameters
    
    frequency_array, horizon_waveform = get_fd_horizon_waveform(horizon_params)
    transfer_function = get_fd_total_transfer(input_parameters)[1]

    return frequency_array, horizon_waveform*transfer_function

def get_fd_upto_n_echoes(template = None, **kwargs):
    """
    Generating the up to n echoes waveform in frequency domain as described in arXiv:1706.06155

    Parameters
    ----------
    template: object
        An object that has attached properties. This can be used to substitute for keyword arguments. A common example would be a row in an xml table.
    
    alpha_plus: complex
        The complex amplitude for the sinusoids at the positive QNM (Quasi-Normal Mode) frequency (i.e. \Omega_{+})
    alpha_minus: complex
        The complex amplitude for the sinusoids at the negative QNM frequency (i.e \Omega_{-})
    t_s: float
        The central start time of the quasi-normal mode
    beta: float
        The frequency width of the gaussian
    omega_r: float
        The real part of the QNM frequency, corresponding to the frequency of the sinusoid
    omega_i: float
        The imaginary part of the QNM frequency, corresponding to the damping frequency (1/tau)

    r_bh: function pointer, should accept frequency as the only argument
        Reflection coefficient of the (final) black hole. In general this should be a function of frequency!
    t_bh: function pointer
        Transmission coefficient of the black hole. In general this should be a function of frequency!
    r: function pointer
        Reflection coefficient of the ECO (exotic compact objects). In general this should be a function of frequency!
    x0: float
        Surface of the ECO in tortoise coordinate x = r + 2M ln((r - 2M)/M)
    n: integer
        The number of echoes

    f_lower: float
        The starting frequency (Hz) of the output frequency series
    f_final: float
        The final frequency (Hz) of the output frequency series
    delta_f: float
        The frequency interval between each sample
    """
    input_parameters = props(template, ["alpha_plus", "alpha_minus", "t_s", "beta", "omega_r", "omega_i", "r_bh", "t_bh", "r", "x0", "n", "f_lower", "f_final", "delta_f"], **kwargs)
    n = input_parameters["n"] 
    horizon_params = copy.deepcopy(input_parameters) # Make a deep copy of input parameters
    frequency_array, horizon_waveform = get_fd_horizon_waveform(horizon_params)
    if n == 0:
        return frequency_array, np.arange(input_parameters["f_lower"], input_parameters["f_final"], input_parameters["delta_f"])*(0.0+0.0j)
    transfer_function = get_fd_nth_transfer(template, n = 1)[1]
    for i in range(2, n + 1):
       transfer_function += get_fd_nth_transfer(template, n = i)[1]

    return frequency_array, horizon_waveform*transfer_function
