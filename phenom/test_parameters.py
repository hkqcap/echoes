#!/usr/bin/env python2.7
"""
test_parameters.py: Testing whether the parameter is intrinsic or extrinsic
Copyright (C) 2017  Rico Ka Lok Lo

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""

import phenom_echoes_waveform_tapir
from phenom_echoes_waveform_nakano import get_fd_reflection_rate, get_fd_transmission_rate
from convert_SI_geometrized_unit import convert_mass_SI_to_geometrized
import numpy as np
import copy

def test(free_parameters, template, waveform_generator):
    # Varying one of the free parameters while keeping other parameters the same
    for varying_free_parameter in free_parameters:
        fixed_free_parameters = copy.copy(free_parameters)
        fixed_free_parameters.remove(varying_free_parameter)

        for fixed_free_parameter in fixed_free_parameters:
            template.update({fixed_free_parameter["name"]: fixed_free_parameter["range"][0]})

        waveforms = []
        for new_value in varying_free_parameter["range"]:
            template.update({varying_free_parameter["name"]: new_value})
            waveforms.append(waveform_generator(template, alpha_plus = template["alpha_r"] + 1.0j*template["alpha_i"], alpha_minus = template["alpha_r"] - 1.0j*template["alpha_i"])[1])
        print("Varying {0}:".format(varying_free_parameter["name"]))
        print waveforms/waveforms[np.random.randint(0, len(waveforms))]

# The free parameters of the horizon fd waveform
alpha_r_parameter = {"name": "alpha_r", "range": [a for a in np.linspace(0.00001, 0.01, num = 10)]}
alpha_i_parameter = {"name": "alpha_i", "range": [a for a in np.linspace(0.00001, 0.01, num = 10)]}
t_s_parameter = {"name": "t_s", "range": [a for a in np.linspace(0, 1, num = 10)]}
beta_parameter = {"name": "beta", "range": [a for a in np.linspace(10,200, num = 10)]}

print("Testing horizon waveform")
free_parameters = [alpha_r_parameter, alpha_i_parameter, t_s_parameter, beta_parameter]

# Test template from the paper
template = {
"omega_r": 2.0*np.pi*250,
"omega_i": -1.0/(4e-3),
"r_bh": lambda f: get_fd_reflection_rate(60, f, 0.68),
"t_bh": lambda f: get_fd_transmission_rate(60, f, 0.68),
"r": 0.7,
"n": 3,
"f_lower": +15.0,
"f_final": +1024.0,
"delta_f": 1.0/4.0
}

test(free_parameters, template, phenom_echoes_waveform_tapir.get_fd_horizon_waveform)

# The free parameters of the total echoes
r_parameter = {"name": "r", "range": [a for a in np.linspace(0.01, 0.99, num = 10)]}
x0_parameter = {"name": "x0", "range":[-a*convert_mass_SI_to_geometrized(60, "length") for a in np.linspace(3, 100, num = 10)]}

free_parameters.append(r_parameter)
free_parameters.append(x0_parameter)

template.update({"r_bh": 1.0/np.sqrt(2),"t_bh": 1.0/np.sqrt(2)})

print("Testing total echoes waveform")
test(free_parameters, template, phenom_echoes_waveform_tapir.get_fd_total_echoes)
