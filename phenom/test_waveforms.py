import phenom_echoes_waveform_tapir
from phenom_echoes_waveform_nakano import get_fd_reflection_rate, get_fd_transmission_rate
from convert_SI_geometrized_unit import convert_mass_SI_to_geometrized
import numpy as np
import matplotlib as mpl
mpl.use("Agg")
from matplotlib import pyplot as plt

import scipy.signal
from pycbc.types import TimeSeries, FrequencySeries


def schwarzchild_to_tortoise(r_coor, mass):
    """
    Converting schwarzchild coordinate to tortoise coordinate defined in arXiv:1706.06155

    Parameters
    ----------
    r_coor: float
    The radial part of the schwarzchild coordinate
    mass: float
    The mass of the system in solar mass
    """
    factor = 2*convert_mass_SI_to_geometrized(mass, "length")
    return r_coor + factor*np.log(np.abs((r_coor/factor) - 1)) + factor*np.log(2)

"""
"alpha_plus": -0.00178 + 1.0j*0.00450,
"alpha_minus": -0.042 + 1.0j*0.00905,
"""
alpha_r = -0.0000178
alpha_i = 0.0000450

# Test template from the paper
template = {
"alpha_plus": alpha_r + 1.0j*alpha_i,
"alpha_minus": alpha_r - 1.0j*alpha_i,
"t_s": 0.2,
"beta": 140,
"omega_r": 2.0*np.pi*250,
"omega_i": -1.0/(4e-3),
"r_bh": lambda f: get_fd_reflection_rate(60, f, 0.68),
"t_bh": lambda f: get_fd_transmission_rate(60, f, 0.68),
"r": 0.7,
"x0": -3*convert_mass_SI_to_geometrized(60, "length"),
"n": 3,
"f_lower": +15.0,
"f_final": +1024.0,
"delta_f": 1.0/4.0
}

# Testing the horizon waveform
plt.figure()
frequency_array, h_array = phenom_echoes_waveform_tapir.get_fd_horizon_waveform(template)
h_array *= scipy.signal.tukey(len(h_array), alpha = 0.01)
plt.autoscale()
plt.plot(frequency_array, np.real(h_array), label = r"$\Re(Z_{T}^{H})$")
plt.plot(frequency_array, np.imag(h_array), label = r"$\Im(Z_{T}^{H})$")
plt.plot(frequency_array, np.absolute(h_array), label = r"$|Z_{T}^{H}|$")
plt.xlabel("Frequency (Hz)")
plt.ylabel(r"$Z$")
plt.title("Horizon waveform in frequency-domain")
plt.grid()
plt.legend()
plt.savefig("template_horizon_fd.png")

# Testing the transfer function

# Testing the reflection and transmission amplitude fitting 
reflection_amplitude = [template["r_bh"](freq) for freq in frequency_array]
transmission_amplitude = [template["t_bh"](freq) for freq in frequency_array]
plt.figure()
plt.xlabel("Frequency (Hz)")
plt.plot(frequency_array, reflection_amplitude, label = r"$\tilde{\mathcal{R}}_{BH}(f)$")
plt.plot(frequency_array, transmission_amplitude, label = r"$\tilde{\mathcal{T}}_{BH}(f)$")
plt.grid()
plt.legend()
plt.savefig("reflection_transmission_amplitude.png")

# Testing the echo waveform
plt.figure()
frequency_array, h_array = phenom_echoes_waveform_tapir.get_fd_upto_n_echoes(template)
h_array *= scipy.signal.tukey(len(h_array), alpha = 0.01)
plt.plot(frequency_array, np.real(h_array), label = r"$\Re(Z_{T})$")
plt.plot(frequency_array, np.imag(h_array), label = r"$\Im(Z_{T})$")
plt.xlabel("Frequency (Hz)")
plt.ylabel(r"$Z$")
plt.title("Echo waveform up to {0}th echo".format(template["n"]))
plt.grid()
plt.legend()
plt.savefig("template_echo_fd.png")

h_frequencyseries = FrequencySeries(h_array, delta_f = template["delta_f"])
h_timeseries = h_frequencyseries.to_timeseries()

plt.figure()
plt.plot(h_timeseries.sample_times, h_timeseries, label = "Raw h(t)")
print h_timeseries.sample_rate
plt.xlabel("Time (s)")
plt.ylabel(r"$h(t)$")
plt.grid()
plt.savefig("template_echo_td.png")
