import pandas
import numpy as np
import scipy.signal
import pycbc
from pycbc.types import TimeSeries
import matplotlib as mpl
mpl.use("Agg")
from matplotlib import pyplot as plt
from optparse import OptionParser

G = 6.67408e-11
c = 299792458.0
MSUN = 1.98855e30

def get_ringdown_from_bh_waveform(bh_waveform, t_merger, mass, epoch, delta_t):
	"""
	Return the ringdown part of the black hole waveform

	t_ringdown = t_merger + 10GM/c^3
	"""
	t_ringdown = t_merger + 10.0*G*mass/(c*c*c)
	print "t_ringdown =", t_ringdown, "s"
	idx = int((t_ringdown - epoch)*delta_t)
	return bh_waveform[idx:]

def load_bh_waveform(filename):
	nr_waveform_dataframe = pandas.read_csv(filename, header = None, delimiter = " ")
	return nr_waveform_dataframe

def tapering(waveform):
	"""
	Applying a Tukey window to the signal
	"""
	window = scipy.signal.tukey(len(waveform), alpha = 0.01)
	return window*waveform

def plot_waveform(x_vector, y_vector, filename, title, xlabel, ylabel):
	plt.figure()
	plt.grid()
	plt.plot(x_vector, y_vector)
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	plt.title(title)
	plt.savefig(filename)


parser = OptionParser()
(options, filename) = parser.parse_args()

nr_waveform_dataframe = load_bh_waveform(filename[0])
ringdown_waveform_dataframe = get_ringdown_from_bh_waveform(nr_waveform_dataframe, 0.0, 62*MSUN, -6.204041089999999814e-01, 4096)
ringdown_timeseries = TimeSeries(tapering(ringdown_waveform_dataframe[1].as_matrix()), delta_t = 1.0/4096)
ringdown_frequencyseries = ringdown_timeseries.to_frequencyseries()

plot_waveform(ringdown_frequencyseries.sample_frequencies, np.abs(np.real(ringdown_frequencyseries)), "GW150914_ringdown_fd_abs.png", "Ringdown part of GW150914", "Frequency (Hz)", r"$|h_{+}(f)|$ (1/rHz)")

plot_waveform(ringdown_frequencyseries.sample_frequencies, np.real(ringdown_frequencyseries), "GW150914_ringdown_fd.png", "Ringdown part of GW150914 in frequency domain", "Frequency (Hz)", r"$|h_{+}(f)|$ (1/rHz)")

plot_waveform(ringdown_timeseries.sample_times, ringdown_timeseries, "GW150914_ringdown_td_tukey.png", "Ringdown part of GW150914 event in time domain with Tukey windows", "Time from merger (s)", r"$h(t)$")

plot_waveform(ringdown_waveform_dataframe[0], ringdown_waveform_dataframe[1], "GW150914_ringdown_td_no_tapering.png", "Ringdown part of GW150914 event in time domain without tapering", "Time from merger (s)", r"$h(t)$")
