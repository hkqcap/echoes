import pandas
import numpy as np
import pycbc
from pycbc.types import TimeSeries
import matplotlib as mpl
mpl.use("Agg")
from matplotlib import pyplot as plt
from optparse import OptionParser

G = 6.67408e-11
c = 299792458.0
MSUN = 1.98855e30

sampling_rate = 4096
gw150914_final_mass = 62
gw150914_gpstime = 1126259462

beta = 0.01
t_s = 0.0

def get_ringdown_from_bh_waveform(bh_waveform, t_merger, mass, epoch, delta_t):
	"""
	Return the ringdown part of the black hole waveform

	t_ringdown = t_merger + 10GM/c^3
	"""
	t_ringdown = t_merger + 10.0*G*mass/(c*c*c)
	print "t_ringdown =", t_ringdown, "s"
	idx = int((t_ringdown - epoch)*delta_t)
	return bh_waveform[idx:]

def load_bh_waveform(filename):
	nr_waveform_dataframe = pandas.read_csv(filename, header = None, delimiter = " ")
	return nr_waveform_dataframe

def plot_waveform(x_vector, y_vector, filename, title, xlabel, ylabel):
	plt.figure()
	plt.grid()
	plt.plot(x_vector, y_vector)
	plt.xlabel(xlabel)
	plt.ylabel(ylabel)
	plt.title(title)
	plt.savefig(filename)


parser = OptionParser()
(options, filename) = parser.parse_args()

# Getting the ringdown waveform
nr_waveform_dataframe = load_bh_waveform(filename[0])
ringdown_waveform_dataframe = get_ringdown_from_bh_waveform(nr_waveform_dataframe, 0.0, gw150914_final_mass*MSUN, -6.204041089999999814e-01, sampling_rate)
ringdown_timeseries = TimeSeries(ringdown_waveform_dataframe[1].as_matrix(), delta_t = 1.0/sampling_rate)
ringdown_frequencyseries = ringdown_timeseries.to_frequencyseries()

# Constructing h(t) in the paper (not GW!)
t_vec = np.arange(-1.0, ringdown_waveform_dataframe[0].tail(1), 1.0/sampling_rate)
h_vec = np.exp(((t_vec - t_s)**2)/(2.0/(beta**2)))
h_vec = (beta/np.sqrt(2.0*np.pi))*h_vec
h_timeseries = TimeSeries(h_vec, delta_t = 1.0/sampling_rate)
h_frequencyseries = h_timeseries.to_frequencyseries()

# Need padding before multiplying!!
